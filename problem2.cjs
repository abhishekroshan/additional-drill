function nationality(object) {
  const array = Object.entries(object);

  const users = array.filter((item) => {
    return item[1].nationality === "Germany";
  });
  /*checking if the nationality is germany using filter method on
  the array */

  return users;
}

module.exports = nationality;
