function videoGames(users) {
  const array = Object.keys(users);

  const result = array.filter((value) => {
    if (users[value]["interests"][0].includes("Video Games")) {
      return value;
    }
    //if the index.interest contain "Video Games" we return the value
    //inside the object
  });
  return result;
}

module.exports = videoGames;
