function qualification(obj) {
  const array = Object.entries(obj);

  const result = array.filter((value) => {
    return value[1].qualification === "Masters";
  });
  /* checking the value of the array to see if it has the qualification
  as masters */

  return result;
}

module.exports = qualification;
