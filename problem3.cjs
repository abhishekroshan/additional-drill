function sortedUsers(object) {
  const array = Object.keys(object).sort((a, b) => {
    const priority = {
      "Senior Golang Developer": 3,
      "Senior Javascript Developer": 3,
      "Python Developer": 2,
      "Intern - Golang": 1,
      "Intern - Javascript": 1,
    };
    const comparison =
      priority[object[b].designation] - priority[object[a].designation];
    //comparision with the designation form the object priority created

    const ageComparision = object[b].age - object[a].age;
    //comparision with the age of the users

    if (comparison !== 0) {
      return comparison;
    } else {
      return ageComparision;
    }
    /* If the designation is same we compare it by age to sort */
  });

  return array;
}

module.exports = sortedUsers;
